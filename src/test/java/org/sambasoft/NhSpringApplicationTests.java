package org.sambasoft;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.sambasoft.entities.Employee;
import org.sambasoft.entities.Report;
import org.sambasoft.entities.Task;
import org.sambasoft.entities.User;
import org.sambasoft.services.EmployeeService;
import org.sambasoft.services.ReportService;
import org.sambasoft.services.TaskService;
import org.sambasoft.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NhSpringApplicationTests {

    @Autowired
    private UserService userService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private ReportService reportService;
    @Autowired
    private EmployeeService employeeService;

    @Before
    public void initDb() {
        {
            User newUser = new User("testUser@mail.com", "testUser", "123456");
            userService.createUser(newUser);
        }
        {
            User newUser = new User("testAdmin@mail.com", "testAdmin", "123456");
            userService.createUser(newUser);
        }
        {
            Report newReport = new Report(
                    "In eos esse enim laudantium expedita.",
                    "\"Quas tenetur facere saepe architecto autem. Ea nihil, officiis esse beatae unde totam non architecto repellat ut error! Iste maiores quasi sit esse aut saepe labore excepturi culpa doloribus temporibus. Qui et dolorem molestiae et laboriosam labore blanditiis dolores porro nulla sint; et facere sit, ut aliquam molestias dicta explicabo deserunt aspernatur hic ut. Consequuntur est voluptatem omnis suscipit dolores id laboriosam doloribus illo quas voluptatibus? Exercitationem asperiores expedita, iusto ea id at eligendi aliquid aut tenetur quia.\r\n" +
                            "Et officiis unde tempore, et molestias rerum distinctio eum iste voluptate minima...\"",
                    "07/08/2012 9:26:28",
                    "https://www.eswaoulher.org/oume/all/are/waheyou.php");
            reportService.createReport(newReport);
        }

        Task userTask = new Task("03/01/2018", "00:11", "11:00", "You need to work today");
        User user = userService.findOne("testUser@mail.com");
        taskService.addTask(userTask, user);
    }

    @Test
    public void testUser() {
        User user = userService.findOne("testUser@mail.com");
        assertNotNull(user);
    }

    @Test
    public void testUser2() {
        User admin = userService.findOne("testAdmin@mail.com");
        assertEquals(admin.getEmail(), "testAdmin@mail.com");
    }

    @Test
    public void testTask() {
        User user = userService.findOne("testUser@mail.com");
        List<Task> task = taskService.findUserTask(user);
        assertNotNull(task);
    }

    @Test
    public void testTask2() {
        User admin = userService.findOne("testAdmin@mail.com");
        assertEquals(admin.getEmail(), "testAdmin@mail.com");
    }

    @Test
    public void testReport() {
        Report report = reportService.findOne((long) 11);
        assertNotNull(report);
    }

    @Test
    public void testReport2() {
        Report report1 = reportService.findOne((long) 11);
        assertEquals(report1.getLink(), "https://www.ebay.es/");
    }

    @Test
    public void testEmployee() {
        List<Employee> employee = employeeService.findByName("l");
        assertNotNull(employee);
    }

    @Test
    public void testEmployee2() {
        Employee employee1 = employeeService.findOne(5);
        assertEquals(employee1.getPhone(),"478-3529");
    }

}

