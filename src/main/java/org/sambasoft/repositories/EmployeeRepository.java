package org.sambasoft.repositories;

import org.sambasoft.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    List<Employee> findByNameLike(String name);

    List<Employee> findByNameContaining(String name);

}
