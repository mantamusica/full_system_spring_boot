package org.sambasoft.repositories;

import java.util.List;

import org.sambasoft.entities.Report;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportRepository extends JpaRepository<Report, Long> {

    List<Report> findByTitleLike(String title);

}
