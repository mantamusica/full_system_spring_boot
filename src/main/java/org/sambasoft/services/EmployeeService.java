package org.sambasoft.services;

import org.sambasoft.entities.Employee;

import java.util.List;

public interface EmployeeService {

    List<Employee> search(String q);

    Employee findOne(int id);

    void save(Employee contact);

    void delete(int id);

    List<Employee> findByName(String name);

}
