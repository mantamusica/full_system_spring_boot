package org.sambasoft.services;

import java.util.List;

import org.sambasoft.entities.Report;

public interface ReportService {

    Report findOne(Long id);

    List<Report> findAll();

    List<Report> findByTitle(String title);

    void createReport(Report report);

    void delete(Long id);

    void save(Report report);

}
