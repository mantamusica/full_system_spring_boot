package org.sambasoft.services;

import org.sambasoft.entities.Report;
import org.sambasoft.repositories.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private ReportRepository reportRepository;

    public void createReport(Report report) {
        reportRepository.save(report);
    }

    public Report findOne(Long id) {
        return reportRepository.findOne(id);
    }

    public List<Report> findAll() {
        return reportRepository.findAll();
    }

    public void delete(Long id) {
        reportRepository.delete(id);
    }

    public void save(Report report) {
        reportRepository.save(report);
    }

    public List<Report> findByTitle(String title) {
        return reportRepository.findByTitleLike("%" + title + "%");
    }


}
