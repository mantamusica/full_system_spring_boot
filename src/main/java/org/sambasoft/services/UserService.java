package org.sambasoft.services;

import java.util.List;

import org.sambasoft.entities.User;

public interface UserService {

    void createUser(User user);

    void createAdmin(User user);

    User findOne(String email);

    boolean isUserPresent(String email);

    List<User> findAll();

    List<User> findByName(String name);


}

