package org.sambasoft.services;

import org.sambasoft.entities.Employee;
import org.sambasoft.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService{

    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> search(String q) {
        return employeeRepository.findByNameContaining(q);
    }

    public Employee findOne(int id) {
        return employeeRepository.findOne(id);
    }

    public void save(Employee contact) {
        employeeRepository.save(contact);
    }

    public void delete(int id) {
        employeeRepository.delete(id);
    }

    public List<Employee> findByName(String name) {
        return employeeRepository.findByNameLike("%" + name + "%");
    }

}

