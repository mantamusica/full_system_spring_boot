package org.sambasoft.services;

import java.util.List;

import org.sambasoft.entities.Task;
import org.sambasoft.entities.User;
import org.sambasoft.repositories.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

public interface TaskService {

    void addTask(Task task, User user);

    List<Task> findUserTask(User user);

}
