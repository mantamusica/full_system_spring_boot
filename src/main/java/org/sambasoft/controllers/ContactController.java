package org.sambasoft.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ContactController {


    @GetMapping("/contact")
    public String showContactPage() {
        return "views/contact";
    }

    @PostMapping("/contact")
    public String registerUser(BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "views/contact";
        }
        return "index";
    }

}
