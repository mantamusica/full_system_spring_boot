package org.sambasoft.controllers;

import org.sambasoft.entities.Report;
import org.sambasoft.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class ReportController {

    @Autowired
    ReportService reportService;

    @GetMapping("/report")
    public String listReports(Model model, @RequestParam(defaultValue = "") String title) {
        model.addAttribute("reports", reportService.findByTitle(title));
        return "views/report_list";
    }

    @GetMapping("/addReport")
    public String create(Model model) {
        model.addAttribute("report", new Report());
        return "views/report_form";
    }

    @GetMapping("/deleteReport/{id}")
    public String delete(@PathVariable("id") Long id, RedirectAttributes redirect) {
        reportService.delete(id);
        redirect.addFlashAttribute("success", "Deleted report successfully!");
        return "redirect:/report";
    }

    @RequestMapping(path = "/editReport/{id}")
    public String edit(@PathVariable("id") Long id , Model model) {
        model.addAttribute("report", reportService.findOne(id));
        return "views/report_form_edit";
    }

    @PostMapping("/saveReport")
    public String save(@Valid Report report, BindingResult result, RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return "views/report_form";
        }
        reportService.save(report);
        redirect.addFlashAttribute("success", "Saved report successfully!");
        return "redirect:/report";
    }
}
