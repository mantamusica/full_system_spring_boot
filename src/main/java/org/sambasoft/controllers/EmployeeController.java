package org.sambasoft.controllers;

import javax.validation.Valid;

import org.sambasoft.entities.Employee;
import org.sambasoft.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employee")
    public String listEmployees(Model model, @RequestParam(defaultValue = "") String name) {
        model.addAttribute("employees", employeeService.findByName(name));
        return "views/employee_list";
    }

    @GetMapping("/addEmployee")
    public String create(Model model) {
        model.addAttribute("employee", new Employee());
        return "views/employee_form";
    }

    @GetMapping("/editEmployee/{id}/")
    public String edit(@PathVariable int id, Model model) {
        model.addAttribute("employee", employeeService.findOne(id));
        return "views/employee_form";
    }

    @PostMapping("/saveEmployee")
    public String save(@Valid Employee employee, BindingResult result, RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return "views/employee_form";
        }
        employeeService.save(employee);
        redirect.addFlashAttribute("success", "Saved employee successfully!");
        return "redirect:/employee";
    }

    @GetMapping("/deleteEmployee/{id}")
    public String delete(@PathVariable int id, RedirectAttributes redirect) {
        employeeService.delete(id);
        redirect.addFlashAttribute("success", "Deleted employee successfully!");
        return "redirect:/employee";
    }

    @GetMapping("/employee/search")
    public String search(@RequestParam("s") String s, Model model) {
        if (s.equals("")) {
            return "redirect:/employee";
        }

        model.addAttribute("employees", employeeService.search(s));
        return "views/employee_list";
    }
}
